export const corsOptions = {
    origin: 'ALL',
	methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH'],
	credentials: true,
};

