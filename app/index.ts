import express, { Application } from 'express';
import cors from 'cors';
import { corsOptions } from './config/cors';
import { routes } from './routes/routes';
import { sequelize } from './database/sequelize';

const app: Application = express();
const port = 5000;

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/', routes);
sequelize.sync();

app.listen(port, () => console.log(`Server is listening on port ${port}!`));