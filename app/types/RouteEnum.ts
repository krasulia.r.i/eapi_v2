export enum RouteEnum {
    CUSTOMER = '/customer',
    CUSTOMERS = '/customers',
    ORDERS = '/orders',
    ORDER = '/order/:orderID',
}