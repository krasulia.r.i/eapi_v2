import { Request, Response } from 'express';
import { User } from '../models/user';
import { Customer } from '../models/customer';

export const createCustomer = (req: Request, res: Response) => {
  res.status(200).json({message: 'Customer created'});
};

export const getCustomer = async (req: Request, res: Response) => {
  try {
    let data = await Customer.findAll();
    console.log(data, ' => data');
  } catch (e) {
    console.log(e, ' => error');
  }
  res.status(200).json({message: 'GET customer'});
};

