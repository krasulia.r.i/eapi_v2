import { Router } from "express"
import { getCustomer } from "../controllers/customersController";
import { RouteEnum } from "../types/RouteEnum";

export const сustomersRoute = Router();

сustomersRoute.get(RouteEnum.CUSTOMER, getCustomer);
