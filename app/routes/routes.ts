import { Router } from "express";
import { сustomersRoute } from './customersRoutes';

export const routes = Router();

routes.get('/', (_, res) => res.json({ message: 'Welcome to e-api.' }))
routes.use(сustomersRoute);