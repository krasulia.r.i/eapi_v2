import { Model, DataTypes, Optional } from 'sequelize'
import { sequelize } from '../database/sequelize';

export interface CustomerAddressesAttributes {
    id: number;
    city: string;
    street: string;
    home: string;
    apartment: string;
    entrance: string;
    floor: string;
    doorphone: string;
    housing: string;
    comment: string;
};

interface CustmerCreationAttributes
  extends Optional<CustomerAddressesAttributes, 'id'> {};

interface CustmerCreationInstance
  extends Model<CustomerAddressesAttributes, CustmerCreationAttributes>, 
  CustomerAddressesAttributes {
      createdAt?: Date;
      updatedAt?: Date;
    };

export const Customer = sequelize.define<CustmerCreationInstance>('CustomerAddresses', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    city: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    street: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    home: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    apartment: {
        type: DataTypes.STRING,
    },
    floor: {
        type: DataTypes.STRING,
    },
    entrance: {
        type: DataTypes.STRING,
    },
    doorphone: {
        type: DataTypes.STRING,
    },
    housing: {
        type: DataTypes.STRING,
    },
    comment: {
        type: DataTypes.STRING,
    },
}, {
    tableName: "CustomerAddresses",
    createdAt: 'updateTimestamp',
    timestamps: true,
});