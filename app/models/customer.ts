import { Model, DataTypes, Optional } from 'sequelize'
import { sequelize } from '../database/sequelize';

export interface CustomerAttributes {
    id: number;
    name: string;
    phone: string;
    email?: string;
    birthday?: Date;
    sex?: string;
    image?: string;
    isBlocked?: boolean;
};

interface CustmerCreationAttributes
  extends Optional<CustomerAttributes, 'id'> {};

interface CustomerInstance
  extends Model<CustomerAttributes, CustmerCreationAttributes>, 
  CustomerAttributes {
      createdAt?: Date;
      updatedAt?: Date;
    };

export const Customer = sequelize.define<CustomerInstance>('Customer', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: DataTypes.STRING,
    phone: {
        type: DataTypes.STRING(32),
        allowNull: false,
    },
    birthday: DataTypes.DATE,
    sex: DataTypes.STRING,
    image: DataTypes.STRING,
    isBlocked: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
    },
}, {
    tableName: "Customer",
    createdAt: 'updateTimestamp',
    timestamps: true,
});