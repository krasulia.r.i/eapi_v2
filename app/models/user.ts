import { Model, DataTypes, Optional } from 'sequelize'
import { sequelize } from '../database/sequelize';

export interface UserAttributes {
    id: number;
    email: string;
    password: string;
};

interface UserCreationAttributes
  extends Optional<UserAttributes, 'id'> {};

interface UserInstance
  extends Model<UserAttributes, UserCreationAttributes>, 
    UserAttributes {
      createdAt?: Date;
      updatedAt?: Date;
    };

export const User = sequelize.define<UserInstance>('User', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email: DataTypes.STRING,
    password: DataTypes.STRING
}, {
    tableName: "User",
});